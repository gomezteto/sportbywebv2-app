## Table of Contents

* [Versions](#versions)
* [Quick Start](#quick-start)
* [Documentation](#documentation)
* [File Structure](#file-structure)
* [Licensing](#licensing)

## Version
- 0.0.1

## Quick start
- Try it out on Expo (Simulator for iOS or even your physical device if you have an Android)
- run `npm install`
- run `npm start`
- open Expo on your phone and scan the QR code

## Documentation
The documentation for the React Native Seed ADSA is hosted at our [website](http://adsa.co/).

## File Structure
Within the download you'll find the following directories and files:

```
react-native-seed-adsa/
├── App.js
├── README.md
├── assets
├──  src
│   ├── components
│   │   ├── Button.js
│   │   ├── Drawer.js
│   │   ├── Header.js
│   │   ├── Icon.js
│   │   ├── Select.js
│   │   ├── Switch.js
│   │   ├── Tabs.js
│   │   └── index.js
│   ├── constants
│   │   ├── Images.js
│   │   ├── Theme.js
│   │   ├── index.js
│   │   └── utils.js
│   ├── navigation
│   │   ├── Menu.js
│   │   └── Screens.js
│   ├── models
│   │   ├── loading
│   │   ├── user
│   │   └── index.js
│   └── screens
│       ├── Components.js
│       ├── Home.js
│       ├── Profile.js
│       └── Settings.js
├── babel.config.js
├── package-lock.json
├── app.json
└── package.json
```

## Licensing

- Copyright 2020 ADSA (http://adsa.co/)

