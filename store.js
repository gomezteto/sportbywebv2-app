import { init } from '@rematch/core'
import createPersistPlugin from '@rematch/persist'

import * as models from './src/models'

const persistPlugin = createPersistPlugin({
  whitelist: ['user']
})

const store = init({
  models,
  plugins: [persistPlugin]
})

export default store
