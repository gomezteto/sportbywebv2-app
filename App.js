import React, { useState } from 'react'
import { Platform, StatusBar, Image } from 'react-native'
import { AppLoading } from 'expo'
import { Asset } from 'expo-asset'
import { Provider } from 'react-redux'
import { Block, GalioProvider } from 'galio-framework'

import { Images, products, materialTheme } from './src/constants/'

import { NavigationContainer } from '@react-navigation/native'
import Screens from './src/navigation/Screens'
import store from './store'

// Before rendering any navigation stack
import { enableScreens } from 'react-native-screens'
enableScreens()

// cache app images
const assetImages = [
  Images.Pro,
  Images.LogIn,
  Images.Profile,
  Images.Avatar,
  Images.Onboarding
]

// cache product images
products.map(product => assetImages.push(product.image))

function cacheImages(images) {
  return images.map(image => {
    if (typeof image === 'string') {
      return Image.prefetch(image)
    } else {
      return Asset.fromModule(image).downloadAsync()
    }
  })
}

const App = props => {
  const [isLoadingComplete, setIsLoadingComplete] = useState(false)

  const _loadResourcesAsync = async () => {
    return Promise.all([...cacheImages(assetImages)])
  }

  const _handleLoadingError = error => {
    // In this case, you might want to report the error to your error
    // reporting service, for example Sentry
    console.warn(error)
  }

  const _handleFinishLoading = () => {
    setIsLoadingComplete(true)
  }

  if (!isLoadingComplete && !props.skipLoadingScreen) {
    return (
      <AppLoading
        startAsync={_loadResourcesAsync}
        onError={_handleLoadingError}
        onFinish={_handleFinishLoading}
      />
    )
  }

  return (
    <NavigationContainer>
      <Provider store={store}>
        <GalioProvider theme={materialTheme}>
          <Block flex>
            {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
            <Screens />
          </Block>
        </GalioProvider>
      </Provider>
    </NavigationContainer>
  )
}

export default App
