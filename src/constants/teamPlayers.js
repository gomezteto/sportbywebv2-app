export default [
  {
    name: 'Laura Retana',
    image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcS2wFK0IryapFeN5H7KSSwvfV5zrGY5367gUba4lXcI8Gqwsd_K',
    position: 'Portera',
  },
  {
    name: 'Teto Gómez',
    image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR-4YerdSQg3C-OrP6P8XyaRrwYdti1VXxvloMmEvQ7fvxFQME6',
    position: 'Defensa',
  },
  {
    name: 'Arbisu Arbisu',
    image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR-4YerdSQg3C-OrP6P8XyaRrwYdti1VXxvloMmEvQ7fvxFQME6',
    position: 'Volante',
  },
  {
    name: 'Adriel Diaz',
    image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR-4YerdSQg3C-OrP6P8XyaRrwYdti1VXxvloMmEvQ7fvxFQME6',
    position: 'Delantero',
  },
  {
    name: 'Christian Abarca',
    image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR-4YerdSQg3C-OrP6P8XyaRrwYdti1VXxvloMmEvQ7fvxFQME6',
    position: 'Delantero',
  },
];
