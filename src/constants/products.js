export default [
  {
    title: 'ML Sports',
    image: 'https://barriofut.com/system/images/files/000/000/070/listing/13062487_260283790981458_1145532327355206712_n.jpg?1470353348',
    price: '8384 1559',
    horizontal: true,
  },
  {
    title: 'Multicanchas Alcazares',
    image: 'https://barriofut.com/system/images/files/000/000/096/listing/95D56250-553B-4B40-9453-8DCBCDA959B2.jpg?1473128109',
    price: '2269 8080',
  },
  {
    title: 'Complejo Sport Soccer',
    image: 'https://barriofut.com/system/images/files/000/000/106/listing/olivier2.jpg?1473132245',
    price: '2203 4570',
  },
  {
    title: 'Sport Center Cartago',
    image: 'https://barriofut.com/system/images/files/000/000/139/listing/IMG_0093_%282%29.JPG?1473637700',
    price: '2236 6671',
    horizontal: true,
  },
  {
    title: 'Complejo Furati',
    image: 'https://barriofut.com/system/images/files/000/000/111/listing/Canchas_Futbol.jpg?1473132928',
    price: '2431 0500',
  },
];
