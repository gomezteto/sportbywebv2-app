import React, { useState } from 'react'
import {
  ImageBackground,
  StyleSheet,
  Dimensions,
  Platform,
  KeyboardAvoidingView,
  Alert
} from 'react-native'
import { Block, Button, Text, theme, Input } from 'galio-framework'
import { LinearGradient } from 'expo-linear-gradient'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { Images, materialTheme } from '../constants'
import { HeaderHeight } from '../constants/utils'

const { height, width } = Dimensions.get('screen')

const SignIn = ({ navigation, login, currentUser }) => {
  const [inputs, setInputs] = useState({ email: null, password: null })
  const [error, setError] = useState(null)
  const [inputValidation, setInputValidation] = useState({
    email: null,
    password: null
  })

  const handleLogin = async () => {
    navigation.navigate('App')

    // const isEmailEmpty = !inputs.email || !inputs.email.length
    // const isPasswordEmpty = !inputs.password || !inputs.password.length

    // if (isEmailEmpty || isPasswordEmpty) {
    //   setInputValidation({ email: isEmailEmpty, password: isPasswordEmpty })

    //   return
    // }

    // const result = await login(inputs)

    // if (result !== 'invalid') {
    //   navigation.navigate('App')
    // } else {
    //   setError('algo pasó papeeee!')
    // }
  }

  const handleOnChangeText = (inputName, value) => {
    setInputs({ ...inputs, [inputName]: value })
    const isNotEmpty = value || value.length
    setInputValidation({ ...inputValidation, [inputName]: !isNotEmpty })
  }

  return (
    <Block top style={styles.container}>
      <KeyboardAvoidingView behavior="height" enabled>
        <Block flex center>
          <Block style={styles.padded}>
            <Block style={{ paddingHorizontal: theme.SIZES.BASE }}>
              <Block row center space="between">
                <Block flex middle right>
                  <Button
                    round
                    onlyIcon
                    shadowless
                    icon="facebook"
                    iconFamily="font-awesome"
                    iconColor={theme.COLORS.WHITE}
                    iconSize={theme.SIZES.BASE * 1.625}
                    color={theme.COLORS.FACEBOOK}
                    style={[styles.social, styles.shadow]}
                  />
                </Block>
                <Block flex middle center>
                  <Button
                    round
                    onlyIcon
                    shadowless
                    icon="twitter"
                    iconFamily="font-awesome"
                    iconColor={theme.COLORS.WHITE}
                    iconSize={theme.SIZES.BASE * 1.625}
                    color={theme.COLORS.TWITTER}
                    style={[styles.social, styles.shadow]}
                  />
                </Block>
                <Block flex middle left>
                  <Button
                    round
                    onlyIcon
                    shadowless
                    icon="dribbble"
                    iconFamily="font-awesome"
                    iconColor={theme.COLORS.WHITE}
                    iconSize={theme.SIZES.BASE * 1.625}
                    color={theme.COLORS.DRIBBBLE}
                    style={[styles.social, styles.shadow]}
                  />
                </Block>
              </Block>
            </Block>

            <Block
              row
              style={{
                marginTop: theme.SIZES.BASE * 0.5,
                marginBottom: theme.SIZES.BASE * 1
              }}
            ></Block>
            <Block middle>
              <Text size={16} color="rgba(255,255,255,0.6)">
                or be classical
              </Text>
            </Block>

            <Block
              flex
              style={{
                marginTop: theme.SIZES.BASE * 0.5,
                marginBottom: theme.SIZES.BASE * 4
              }}
            >
              <Input
                placeholder="email"
                onChangeText={text => handleOnChangeText('email', text)}
                style={inputValidation.email ? { borderColor: 'red' } : {}}
              />
              {inputValidation.email && (
                <Block left>
                  <Text size={12} italic color="rgba(255,255,255,0.6)">
                    This field is required
                  </Text>
                </Block>
              )}
              <Input
                placeholder="password"
                password
                viewPass
                onChangeText={text => handleOnChangeText('password', text)}
                style={inputValidation.password ? { borderColor: 'red' } : {}}
              />
              {inputValidation.password && (
                <Block left>
                  <Text size={12} italic color="rgba(255,255,255,0.6)">
                    This field is required
                  </Text>
                </Block>
              )}

              <Block right>
                <Text
                  color="rgba(255,255,255,0.6)"
                  size={14}
                  onPress={() => Alert.alert('Not implemented')}
                  style={{
                    alignSelf: 'flex-end',
                    lineHeight: theme.SIZES.FONT * 2
                  }}
                >
                  Forgot your password?
                </Text>
              </Block>
            </Block>

            <Button
              shadowless
              style={styles.button}
              color={materialTheme.COLORS.BUTTON_COLOR}
              onPress={handleLogin}
            >
              SING IN
            </Button>
            <Block middle style={styles.bottonMessage}>
              <Button
                color="transparent"
                shadowless
                onPress={() => Alert.alert('Not implemented')} // navigation.navigate('Register')}
              >
                <Text center color="rgba(255,255,255,0.6)" size={14}>
                  {"Don't have an account? Sign Up"}
                </Text>
              </Button>
            </Block>
          </Block>
        </Block>
        <ImageBackground
          source={{ uri: Images.LogIn }}
          style={{ height: height / 4, width, zIndex: 1 }}
        >
          <LinearGradient
            style={styles.gradient}
            colors={['rgba(0,0,0,0)', 'rgba(0,0,0,1)']}
          />
        </ImageBackground>
      </KeyboardAvoidingView>
    </Block>
  )
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: theme.COLORS.BLACK,
    marginTop: Platform.OS === 'android' ? -HeaderHeight : 0
  },
  shadow: {
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 4,
    shadowOpacity: 0.2,
    elevation: 2
  },
  social: {
    width: theme.SIZES.BASE * 3.5,
    height: theme.SIZES.BASE * 3.5,
    borderRadius: theme.SIZES.BASE * 1.75,
    justifyContent: 'center'
  },
  padded: {
    position: 'absolute',
    bottom:
      Platform.OS === 'android' ? theme.SIZES.BASE * 2 : theme.SIZES.BASE * 3
  },
  button: {
    height: theme.SIZES.BASE * 3,
    shadowRadius: 0,
    shadowOpacity: 0
  },
  bottonMessage: {
    marginTop: theme.SIZES.BASE * 0.5
  },
  gradient: {
    zIndex: 1,
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    height: 66
  }
})

SignIn.propTypes = {
  currentUser: PropTypes.object,
  login: PropTypes.func
}

SignIn.defaultProps = {}

const mapStatetoProps = ({ user }) => ({
  currentUser: user.currentUser
})

const mapDispatchToProps = ({ user }) => ({ login: user.login })

export default connect(mapStatetoProps, mapDispatchToProps)(SignIn)
