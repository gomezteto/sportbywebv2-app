import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, Dimensions, FlatList, Animated } from 'react-native'
import { Block, theme } from 'galio-framework'

const { width } = Dimensions.get('screen')
import materialTheme from '../constants/Theme'

const defaultMenu = [
  { id: 'popular', title: 'Popular' },
  { id: 'beauty', title: 'Beauty' },
  { id: 'cars', title: 'Cars' },
  { id: 'motocycles', title: 'Motocycles' }
]

const MenuHorizontal = ({ initialIndex, data, ...props }) => {
  const [state, setState] = useState({
    active: null
  })

  animatedValue = new Animated.Value(1)

  const animate = () => {
    animatedValue.setValue(0)

    Animated.timing(animatedValue, {
      toValue: 1,
      duration: 300
      // useNativeDriver: true, // color not supported
    }).start()
  }

  menuRef = React.createRef()

  const onScrollToIndexFailed = () => {
    menuRef.current.scrollToIndex({
      index: 0,
      viewPosition: 0.5
    })
  }

  const selectMenu = id => {
    setState({ active: id })

    menuRef.current.scrollToIndex({
      index: data.findIndex(item => item.id === id),
      viewPosition: 0.5
    })

    animate()
    props.onChange && props.onChange(id)
  }

  const renderItem = item => {
    const isActive = state.active === item.id

    const textColor = animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: [
        materialTheme.COLORS.MUTED,
        isActive ? materialTheme.COLORS.ACTIVE : materialTheme.COLORS.MUTED
      ],
      extrapolate: 'clamp'
    })

    const width = animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: ['0%', isActive ? '100%' : '0%'],
      extrapolate: 'clamp'
    })

    return (
      <Block style={styles.titleContainer}>
        <Animated.Text
          style={[styles.menuTitle, { color: textColor }]}
          onPress={() => selectMenu(item.id)}
        >
          {item.title}
        </Animated.Text>
        <Animated.View
          style={{
            height: 2,
            width,
            backgroundColor: materialTheme.COLORS.ACTIVE
          }}
        />
      </Block>
    )
  }

  const renderMenu = () => (
    <FlatList
      {...props}
      data={data}
      horizontal={true}
      ref={menuRef}
      extraData={state}
      keyExtractor={item => item.id}
      showsHorizontalScrollIndicator={false}
      onScrollToIndexFailed={onScrollToIndexFailed}
      renderItem={({ item }) => renderItem(item)}
      contentContainerStyle={styles.menu}
    />
  )

  useEffect(() => {
    initialIndex && selectMenu(initialIndex)
  })

  return (
    <Block style={[styles.container, styles.shadow]}>{renderMenu()}</Block>
  )
}

MenuHorizontal.proptypes = {}

MenuHorizontal.defaultProps = {
  data: defaultMenu,
  initialIndex: null
}

export default MenuHorizontal

const styles = StyleSheet.create({
  container: {
    width: width,
    backgroundColor: theme.COLORS.WHITE,
    zIndex: 2
  },
  shadow: {
    shadowColor: theme.COLORS.BLACK,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    shadowOpacity: 0.2,
    elevation: 4
  },
  menu: {
    paddingHorizontal: theme.SIZES.BASE * 2.5,
    paddingTop: 8,
    paddingBottom: 0
  },
  titleContainer: {
    alignItems: 'center'
  },
  menuTitle: {
    fontWeight: '300',
    fontSize: 16,
    lineHeight: 28,
    // paddingBottom: 8,
    paddingHorizontal: 16,
    color: materialTheme.COLORS.MUTED
  }
})
