import React, { useEffect, useState } from 'react'
import * as Font from 'expo-font'
import { createIconSetFromIcoMoon } from '@expo/vector-icons'
import { Icon } from 'galio-framework'

import GalioConfig from '../../assets/fonts/galioExtra'

const GalioExtra = require('../../assets/fonts/galioExtra.ttf')
const IconGalioExtra = createIconSetFromIcoMoon(GalioConfig, 'GalioExtra')

const IconExtra = ({ name, family, ...rest }) => {
  const [state, setState] = useState({
    fontLoaded: false
  })

  useEffect(() => {
    async function loadFont() {
      await Font.loadAsync({ GalioExtra: GalioExtra })
      setState({ fontLoaded: true })
    }

    loadFont()
  }, [])

  if (name && family && state.fontLoaded) {
    if (family === 'GalioExtra') {
      return <IconGalioExtra name={name} family={family} {...rest} />
    }
    return <Icon name={name} family={family} {...rest} />
  }

  return null
}

export default IconExtra
