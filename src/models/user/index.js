// import apolloClient from 'services/graphql'
import mock from '../../api'

const initialState = {
  currentUser: null,
}

const User = {
  state: initialState,
  reducers: {
    addCurrentUser (state, currentUser) {
      return {
        ...state,
        currentUser
      }
    },
  },
  effects: dispatch => ({
    async login ({ email, password }, state) {
      try {
        dispatch.isLoading.storeIsContentLoading(true)

        const result = await mock.login(email, password)

        if(result !== 'invalid') {
          this.addCurrentUser({ name: 'Tetito', lastName: 'Bonito' })
        } else {
          return result
        }

        dispatch.isLoading.storeIsContentLoading(false)
      } catch (error) {
        console.error('login', error)
        dispatch.isLoading.storeIsContentLoading(false)
      }
    }
  })
}

export default User
